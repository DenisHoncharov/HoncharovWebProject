<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: den4ik
  Date: 16.06.2016
  Time: 16:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Личный кабинет</title>
</head>
<body>
<table border="1" width="100%">
    <col col1 width="150px">

    <tr>
        <th class="col1">Навигация по кинотеатру</th>
        <th> Информация о профиле</th>
    </tr>
    <tr>
        <td><p><a href="/app">Главная страничка</a></p></td>
        <td rowspan="150" valign="top">
            Firstname: ${usersDTO.firstname}<br/>
            Lastname: ${usersDTO.lastname} <br/>
            Login: ${usersDTO.login} <br/>
            Password: ${usersDTO.password} <br/>
            Email: ${usersDTO.email} <br/>
            Sex:   ${usersDTO.sex} <br/>
            Birthday: ${usersDTO.birthday}<br/>
            <br/>
            <a href="myTickets">Мои билеты</a>
    </td>
    </tr>
    <tr>
        <td><p><a href="/app/moviepage">Фильмы</a></p></td>
    </tr>
    <tr>
        <td><p><a href="personalAreaPage">Личный кабинет</a></p></td>
    </tr>
</table>

</body>
</html>
