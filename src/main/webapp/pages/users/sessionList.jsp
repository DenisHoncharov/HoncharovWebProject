<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: den4ik
  Date: 20.06.2016
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список доступных сеансов</title>
</head>
<body>
<table border="1" width="100%">
    <col col1 width="150px">

    <tr>
        <th class="col1">Навигация по кинотеатру</th>
        <th >Список доступных сеансов</th>
    </tr>
    <tr><td><p><a href="/app">Главная страничка</a></p></td>
    <td rowspan="150" valign="top" align="center">
        <c:forEach items="${sessionDTOs}" var="session">
            <a href="session?hall_id=${session.hallID}&session_id=${session.id}">${session.id}</a><br/>
        </c:forEach></td></tr>
    <tr><td><p><a href="/app/moviepage">Фильмы</a></p></td></tr>
    <td><p><a href="/app/pages/users/personalAreaPage">Личный кабинет</a></p></td>
</table>
</body>
</html>
