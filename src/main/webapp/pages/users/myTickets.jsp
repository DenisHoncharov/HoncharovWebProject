<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: den4ik
  Date: 29.06.2016
  Time: 19:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Мои билеты</title>
</head>
<body>
<table border="1" width="100%">
    <col col1 width="150px">

    <tr>
        <th class="col1">Навигация по кинотеатру</th>
        <th>Мои билеты</th>
    </tr>
    <tr>
        <td><p><a href="/app">Главная страничка</a></p></td>
        <td rowspan="150" valign="top" align="center">
            <c:forEach items="${ticketsDTOs}" var="ticket">
             Ряд: ${ticket.rowNumber}<br/>
             Место: ${ticket.seatNumber}<br/>
                <br/>
                <br/>

            </c:forEach>
    </tr>
    <tr>
        <td><p><a href="/app/moviepage">Фильмы</a></p></td>
    </tr>
    <td><p><a href="/app/pages/users/personalAreaPage">Личный кабинет</a></p></td>
</table>
</body>
</html>
