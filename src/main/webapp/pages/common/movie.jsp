<%--
  Created by IntelliJ IDEA.
  User: den4ik
  Date: 16.06.2016
  Time: 16:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${movieDTO.title}</title>
</head>
<body>
<table border="1" width="100%">
    <col col1 width="150px">

    <tr>
        <th class="col1">Навигация по кинотеатру</th>
        <th>Детальная информация о фильме</th>
    </tr>
    <tr>
        <td><p><a href="homepage">Главная страничка</a></p></td>
        <td rowspan="150" valign="top" align="center">
            <table width="100%" border="2">
                <tr>
                    <th>Название:</th><th>Описание:</th><th>Сеансы:</th>
                </tr>
                <tr><td><center>${movieDTO.title}</center></td><td>${movieDTO.description}</td><td><center><a href="pages/users/sessionList?movieId=${movieDTO.id}">Список сеансов</a></center></td></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><p><a href="moviepage">Фильмы</a></p></td>
    </tr>
    <tr>
        <td><p><a href="pages/users/personalAreaPage">Личный кабинет</a></p></td>
    </tr>
    </table>
</body>
</html>
