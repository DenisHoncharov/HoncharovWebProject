<%--
  Created by IntelliJ IDEA.
  User: den4ik
  Date: 12.06.2016
  Time: 0:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Фильмы</title>
</head>
<body>
<table border="1" width="100%">
    <col col1 width="150px">

    <tr>
        <th class="col1">Навигация по кинотеатру</th>
        <th>Список Фильмов:</th>
    </tr>
    <tr>
        <td><p><a href="/app">Главная страничка</a></p></td>
        <td rowspan="150" valign="top" align="center">
            <c:forEach items="${movieDTOList}" var="movies">
                <a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.title}</a><br/>
            </c:forEach></td>
    </tr>
    <tr>
        <td><p><a href="moviepage">Фильмы</a></p></td>
    </tr>
    <td><p><a href="pages/users/personalAreaPage">Личный кабинет</a></p></td>
</table>
</body>
</html>
