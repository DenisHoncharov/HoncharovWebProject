package model;


import java.time.LocalDate;

/**
 * Created by den4ik on 04.06.2016.
 */
public class Session extends Entity<Integer> {
    private int muvieID;
    private int hallID;
    private LocalDate startTime;
    private double price;

    public Session() {
    }

    public Session(int muvieID, int hallID, LocalDate startTime, double price) {
        setMuvieID(muvieID);
        setHallID(hallID);
        setStartTime(startTime);
        setPrice(price);
    }

    public int getMuvieID() {
        return muvieID;
    }

    public void setMuvieID(int muvieID) {
        this.muvieID = muvieID;
    }

    public int getHallID() {
        return hallID;
    }

    public void setHallID(int hallID) {
        this.hallID = hallID;
    }

    public LocalDate getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDate startTime) {
        this.startTime = startTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        if (!super.equals(o)) return false;

        Session session = (Session) o;

        if (getMuvieID() != session.getMuvieID()) return false;
        if (getHallID() != session.getHallID()) return false;
        if (Double.compare(session.getPrice(), getPrice()) != 0) return false;
        return getStartTime() != null ? getStartTime().equals(session.getStartTime()) : session.getStartTime() == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + getMuvieID();
        result = 31 * result + getHallID();
        result = 31 * result + (getStartTime() != null ? getStartTime().hashCode() : 0);
        temp = Double.doubleToLongBits(getPrice());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "SessionDTO{" +
                "muvieID=" + getMuvieID() +
                ", hallID=" + getHallID() +
                ", startTime=" + getStartTime() +
                ", price=" + getPrice() +
                '}' + super.toString();
    }
}
