package model;

/**
 * Created by den4ik on 04.06.2016.
 */
public class Tickets extends Entity<Integer> {
    private int userID;
    private int sessionID;
    private int rowNumber;
    private int seatNumber;
    private boolean isSold;

    public Tickets() {
    }

    public Tickets(int userID, int sessionID, int rowNumber, int seatNumber, boolean isSold) {
        setUserID(userID);
        setSessionID(sessionID);
        setRowNumber(rowNumber);
        setSeatNumber(seatNumber);
        setSold(isSold);
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getSessionID() {
        return sessionID;
    }

    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tickets)) return false;
        if (!super.equals(o)) return false;

        Tickets tickets = (Tickets) o;

        if (getUserID() != tickets.getUserID()) return false;
        if (getSessionID() != tickets.getSessionID()) return false;
        if (getRowNumber() != tickets.getRowNumber()) return false;
        if (getSeatNumber() != tickets.getSeatNumber()) return false;
        return isSold() == tickets.isSold();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getUserID();
        result = 31 * result + getSessionID();
        result = 31 * result + getRowNumber();
        result = 31 * result + getSeatNumber();
        result = 31 * result + (isSold() ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "userID=" + getUserID() +
                ", sessionID=" + getSessionID() +
                ", rowNumber=" + getRowNumber() +
                ", seatNumber=" + getSeatNumber() +
                ", isSold=" + isSold +
                '}' + super.toString();
    }
}
