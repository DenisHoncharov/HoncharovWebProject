package model;

/**
 * Created by den4ik on 04.06.2016.
 */
public class Rows extends Entity<Integer> {
    private int rowNumber;
    private int seatQuantity;
    private int hallID;

    public Rows() {
    }

    public Rows(int rowNumber, int seatQuantity, int hallID) {
        this.rowNumber = rowNumber;
        this.seatQuantity = seatQuantity;
        this.hallID = hallID;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatQuantity() {
        return seatQuantity;
    }

    public void setSeatQuantity(int seatQuantity) {
        this.seatQuantity = seatQuantity;
    }

    public int getHallID() {
        return hallID;
    }

    public void setHallID(int hallID) {
        this.hallID = hallID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rows)) return false;

        Rows rows = (Rows) o;

        if (getRowNumber() != rows.getRowNumber()) return false;
        if (getSeatQuantity() != rows.getSeatQuantity()) return false;
        return getHallID() == rows.getHallID();

    }

    @Override
    public int hashCode() {
        int result = getRowNumber();
        result = 31 * result + getSeatQuantity();
        result = 31 * result + getHallID();
        return result;
    }

    @Override
    public String toString() {
        return "RowsDTO{" +
                "rowNumber=" + getRowNumber() +
                ", seatQuantity=" + getSeatQuantity() +
                ", hallID=" + getHallID() +
                '}' + super.toString();
    }
}
