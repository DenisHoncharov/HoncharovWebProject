package model;

import java.time.LocalDate;

public class Movies extends Entity<Integer>{
    private String title;
    private String description;
    private int duration;
    private LocalDate rentStart;
    private LocalDate rentEnd;
    private String genre;
    private int rating;

    public Movies() {
    }

    public Movies(String title, String description, int duration, LocalDate rentStart, LocalDate rentEnd, String genre, int rating) {
        setTitle(title);
        setDescription(description);
        setDuration(duration);
        setRentStart(rentStart);
        setRentEnd(rentEnd);
        setGenre(genre);
        setRating(rating);
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public LocalDate getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalDate rentStart) {
        this.rentStart = rentStart;
    }

    public LocalDate getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(LocalDate rentEnd) {
        this.rentEnd = rentEnd;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movies)) return false;
        if (!super.equals(o)) return false;

        Movies movies = (Movies) o;

        if (getDuration() != movies.getDuration()) return false;
        if (getRating() != movies.getRating()) return false;
        if (getTitle() != null ? !getTitle().equals(movies.getTitle()) : movies.getTitle() != null) return false;
        if (getDescription() != null ? !getDescription().equals(movies.getDescription()) : movies.getDescription() != null)
            return false;
        if (getRentStart() != null ? !getRentStart().equals(movies.getRentStart()) : movies.getRentStart() != null)
            return false;
        if (getRentEnd() != null ? !getRentEnd().equals(movies.getRentEnd()) : movies.getRentEnd() != null)
            return false;
        return getGenre() != null ? getGenre().equals(movies.getGenre()) : movies.getGenre() == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + getDuration();
        result = 31 * result + (getRentStart() != null ? getRentStart().hashCode() : 0);
        result = 31 * result + (getRentEnd() != null ? getRentEnd().hashCode() : 0);
        result = 31 * result + (getGenre() != null ? getGenre().hashCode() : 0);
        result = 31 * result + getRating();
        return result;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + getTitle() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", duration=" + getDuration() +
                ", rentStart=" + getRentStart() +
                ", rentEnd=" + getRentEnd() +
                ", genre='" + getGenre() + '\'' +
                ", rating=" + getRating() +
                "} " + super.toString();
    }
}
