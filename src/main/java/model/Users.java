package model;


import java.time.LocalDate;

public class Users extends Entity<Integer>{
    private String login;
    private String password;
    private String firstname;
    private String lastname;
    private String email;
    private Sex sex;
    private LocalDate birthday;
    private Role role;

    public Users() {
    }

    public Users(String login, String password, String firstname, String lastname, String email, Sex sex, LocalDate birthday, Role role) {
        setLogin(login);
        setPassword(password);
        setFirstname(firstname);
        setLastname(lastname);
        setEmail(email);
        setSex(sex);
        setBirthday(birthday);
        setRole(role);
    }

    public enum Sex{
        MALE, FAMALE;
    }

    public enum Role{
        ADMIN,CASSA,USER,GUEST;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Users)) return false;
        if (!super.equals(o)) return false;

        Users users = (Users) o;

        if (getLogin() != null ? !getLogin().equals(users.getLogin()) : users.getLogin() != null) return false;
        if (getPassword() != null ? !getPassword().equals(users.getPassword()) : users.getPassword() != null)
            return false;
        if (getFirstname() != null ? !getFirstname().equals(users.getFirstname()) : users.getFirstname() != null)
            return false;
        if (getLastname() != null ? !getLastname().equals(users.getLastname()) : users.getLastname() != null)
            return false;
        if (getEmail() != null ? !getEmail().equals(users.getEmail()) : users.getEmail() != null) return false;
        if (getSex() != users.getSex()) return false;
        if (getBirthday() != null ? !getBirthday().equals(users.getBirthday()) : users.getBirthday() != null)
            return false;
        return getRole() == users.getRole();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getFirstname() != null ? getFirstname().hashCode() : 0);
        result = 31 * result + (getLastname() != null ? getLastname().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getSex() != null ? getSex().hashCode() : 0);
        result = 31 * result + (getBirthday() != null ? getBirthday().hashCode() : 0);
        result = 31 * result + (getRole() != null ? getRole().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + getLogin() +
                ", password='" + getPassword() +
                ", firstname='" + getFirstname() +
                ", lastname='" + getLastname() +
                ", email='" + getEmail() +
                ", sex=" + getSex() +
                ", birthday=" + getBirthday() +
                ", role=" + getRole() +
                '}' + super.toString();
    }
}
