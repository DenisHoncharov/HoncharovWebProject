package dao.api;


import java.util.List;

/**
 * Created by Kovantonlenko on 4/5/2016.
 */
public interface Dao<K, T> { // первое К это тот ид который идет в базу Т это классы из модел

    List<T> findAll();

    T findOne(K key);

    T getBy(String fieldName, String value);

    void save(T entity);

    void deleteOne(K key);

    void update(T entity);


}
