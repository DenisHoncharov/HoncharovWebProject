package dao;

import dao.api.Dao;
import dao.impl.*;
import helpers.PropertyHolder;
import model.*;

/**
 * Created by dmitr on 02.06.2016.
 */
public class DaoFactory {
    private static DaoFactory instance = null;

    private Dao<Integer, Movies> movieDao;
    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Rows> rowsDao;
    private Dao<Integer, Session> sessionDao;
    private Dao<Integer, Tickets> ticketsDao;
    private Dao<Integer, Users> usersDao;

    private DaoFactory() {
        loadDaos();
    }

    public static DaoFactory getInstance() {
        if (instance == null) {
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
        if (PropertyHolder.getInstance().isInMemoryDB()) {

        } else {
            movieDao = new MovieDaoImpl();
            hallDao = new HallDaoImpl();
            rowsDao = new RowsDaoImpl();
            sessionDao = new SessionDaoImpl();
            ticketsDao = new TicketsDaoImpl();
            usersDao = new UsersDaoImpl();
        }
    }

    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }

    public void setHallDao(Dao<Integer, Hall> hallDao) {
        this.hallDao = hallDao;
    }

    public Dao<Integer, Rows> getRowsDao() {
        return rowsDao;
    }

    public void setRowsDao(Dao<Integer, Rows> rowsDao) {
        this.rowsDao = rowsDao;
    }

    public Dao<Integer, Session> getSessionDao() {
        return sessionDao;
    }

    public void setSessionDao(Dao<Integer, Session> sessionDao) {
        this.sessionDao = sessionDao;
    }

    public Dao<Integer, Tickets> getTicketsDao() {
        return ticketsDao;
    }

    public void setTicketsDao(Dao<Integer, Tickets> ticketsDao) {
        this.ticketsDao = ticketsDao;
    }

    public Dao<Integer, Users> getUsersDao() {
        return usersDao;
    }

    public void setUsersDao(Dao<Integer, Users> usersDao) {
        this.usersDao = usersDao;
    }

    public Dao<Integer, Movies> getMovieDao() {
        return movieDao;
    }

    public void setMovieDao(Dao<Integer, Movies> movieDao) {
        this.movieDao = movieDao;
    }
}
