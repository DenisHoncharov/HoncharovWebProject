package dao.impl;


import model.Movies;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_MOVIE;
import static dao.SQLs.UPDATE_MOVIE;

/**
 * Created by Kovantonlenko on 4/5/2016.
 */
public final class MovieDaoImpl extends CrudDAO<Movies> {

    private static MovieDaoImpl crudDAO;

    public MovieDaoImpl() {
        super(Movies.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Movies entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MOVIE);
        preparedStatement.setString(1, entity.getTitle());
        preparedStatement.setString(2, entity.getDescription());
        preparedStatement.setInt(3, entity.getDuration());
        preparedStatement.setDate(4, Date.valueOf(entity.getRentStart()));
        preparedStatement.setDate(5, Date.valueOf(entity.getRentEnd()));
        preparedStatement.setString(6, entity.getGenre());
        preparedStatement.setInt(7, entity.getRating());
        preparedStatement.setInt(8, entity.getId());
        return preparedStatement;
    }

    @Override
    public PreparedStatement createInsertStatement(Connection connection, Movies entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_MOVIE, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getTitle());
        preparedStatement.setString(2, entity.getDescription());
        preparedStatement.setInt(3, entity.getDuration());
        preparedStatement.setDate(4, Date.valueOf(entity.getRentStart()));
        preparedStatement.setDate(5, Date.valueOf(entity.getRentEnd()));
        preparedStatement.setString(6, entity.getGenre());
        preparedStatement.setInt(7, entity.getRating());
        return preparedStatement;
    }

    @Override
    public List<Movies> readAll(ResultSet resultSet) throws SQLException {
        List<Movies> result = new LinkedList<>();
        Movies movie = null;
        while (resultSet.next()) {
            movie = new Movies();
            movie.setId(resultSet.getInt("id"));
            movie.setTitle(resultSet.getString("title"));
            movie.setDescription(resultSet.getString("description"));
            movie.setDuration(resultSet.getInt("duration"));
            movie.setRentStart(resultSet.getDate("rent_start").toLocalDate());
            movie.setRentEnd(resultSet.getDate("rent_end").toLocalDate());
            movie.setGenre(resultSet.getString("genre"));
            movie.setRating(resultSet.getInt("rating"));
            result.add(movie);
        }
        return result;
    }

}