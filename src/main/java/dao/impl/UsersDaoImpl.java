package dao.impl;


import model.Users;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_USERS;
import static dao.SQLs.UPDATE_USERS;

/**
 * Created by den4ik on 05.06.2016.
 */
public class UsersDaoImpl extends CrudDAO<Users> {

    private static MovieDaoImpl crudDAO;

    public UsersDaoImpl() {
        super(Users.class);
    }


    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Users entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USERS);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPassword());
        preparedStatement.setString(3, entity.getFirstname());
        preparedStatement.setString(4, entity.getLastname());
        preparedStatement.setString(5, entity.getEmail());
        preparedStatement.setObject(6, entity.getSex().toString());
        preparedStatement.setDate(7, Date.valueOf(entity.getBirthday()));
        preparedStatement.setObject(8, entity.getRole().toString());
        preparedStatement.setInt(9, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Users entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPassword());
        preparedStatement.setString(3, entity.getFirstname());
        preparedStatement.setString(4, entity.getLastname());
        preparedStatement.setString(5, entity.getEmail());
        preparedStatement.setObject(6, entity.getSex().toString());
        preparedStatement.setDate(7, Date.valueOf(entity.getBirthday()));
        preparedStatement.setObject(8, entity.getRole().toString());
        return preparedStatement;
    }

    @Override
    protected List<Users> readAll(ResultSet resultSet) throws SQLException {
        List<Users> usersList = new LinkedList<>();
        Users users = null;
        while (resultSet.next()){
            users = new Users();
            users.setId(resultSet.getInt("id"));
            users.setRole(Users.Role.valueOf(resultSet.getString("role_type")));
            users.setBirthday(resultSet.getDate("birthday").toLocalDate());
            users.setEmail(resultSet.getString("email"));
            users.setFirstname(resultSet.getString("first_name"));
            users.setLastname(resultSet.getString("last_name"));
            users.setLogin(resultSet.getString("login"));
            users.setPassword(resultSet.getString("password"));
            users.setSex(Users.Sex.valueOf(resultSet.getString("sex")));
            usersList.add(users);
        }
        return usersList;
    }
}
