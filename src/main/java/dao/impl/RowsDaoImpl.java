package dao.impl;

import model.Rows;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_ROWS;
import static dao.SQLs.UPDATE_ROWS;

/**
 * Created by den4ik on 05.06.2016.
 */
public class RowsDaoImpl extends CrudDAO<Rows> {

    private static MovieDaoImpl crudDAO;

    public RowsDaoImpl() {
        super(Rows.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Rows entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROWS);
        preparedStatement.setInt(1, entity.getRowNumber());
        preparedStatement.setInt(2, entity.getSeatQuantity());
        preparedStatement.setInt(3, entity.getHallID());
        preparedStatement.setInt(4, entity.getId());
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Rows entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROWS, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, entity.getRowNumber());
        preparedStatement.setInt(2, entity.getSeatQuantity());
        preparedStatement.setInt(3, entity.getHallID());
        return preparedStatement;
    }

    @Override
    protected List<Rows> readAll(ResultSet resultSet) throws SQLException {
        List<Rows> rowsList = new LinkedList<>();
        Rows rows = null;
        while (resultSet.next()) {
            rows = new Rows();
            rows.setId(resultSet.getInt("id"));
            rows.setHallID(resultSet.getInt("hall_id"));
            rows.setRowNumber(resultSet.getInt("row_number"));
            rows.setSeatQuantity(resultSet.getInt("seat_quantity"));
            rowsList.add(rows);
        }
        return rowsList;
    }

}
