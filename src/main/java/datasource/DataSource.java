package datasource;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import helpers.PropertyHolder;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;


public final class DataSource {

    private static ComboPooledDataSource poolConnections;  //connections pool
    private static DataSource dataSource;

    private DataSource() {                                 //при создании объекта этого класса вызывает метод initPollConnections()
        initPollConnections();
    }

    public static synchronized DataSource getInstance() { // создает объект DataSource и возвращает его
        if (dataSource == null) {
            dataSource = new DataSource();
        }
        return dataSource;
    }

    public Connection getConnection() {                 // этим методом мы будем давать конекшины к базе данных
        Connection connection = null;
        try {
            connection = poolConnections.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static void initPollConnections() {         //создает объкта пула конекшинов
        poolConnections = new ComboPooledDataSource();
        PropertyHolder propertyHolder = PropertyHolder.getInstance(); //инициализирует propertyHolder(для меньшего текста)
        try {
            poolConnections.setDriverClass(propertyHolder.getDbDriver()); //loads the jdbc driver

            poolConnections.setJdbcUrl(propertyHolder.getJdbcUrl());        // инициализирует пулл конекшинов
            poolConnections.setUser(propertyHolder.getDbUserLogin());       // инициализирует пулл конекшинов
            poolConnections.setPassword(propertyHolder.getDbUserPassword());// инициализирует пулл конекшинов

            poolConnections.setMinPoolSize(5);                              // конфигурирует пулл конекшинов
            poolConnections.setAcquireIncrement(1);                         // конфигурирует пулл конекшинов
            poolConnections.setMaxPoolSize(100);                            // конфигурирует пулл конекшинов
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }


}
