package service.impl;


import dao.DaoFactory;
import dao.api.Dao;
import dto.MovieDTO;
import mapper.BeanMapper;
import model.Movies;
import service.api.Service;

import java.util.List;

public class MovieServiceImpl implements Service<Integer, MovieDTO> {

    private static MovieServiceImpl service;
    private Dao<Integer, Movies> movieDao;
    private BeanMapper beanMapper;

    private MovieServiceImpl() {
        movieDao = DaoFactory.getInstance().getMovieDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized MovieServiceImpl getInstance() {
        if (service == null) {
            service = new MovieServiceImpl();
        }
        return service;
    }


    @Override
    public List<MovieDTO> getAll() {
        List<Movies> movies = movieDao.findAll();
        List<MovieDTO> movieDTOs = beanMapper.listMapToList(movies, MovieDTO.class);
        return movieDTOs;
    }

    @Override
    public void save(MovieDTO movieDto) {
        Movies movies = beanMapper.singleMapper(movieDto, Movies.class);
        movieDao.save(movies);
    }

    @Override
    public MovieDTO getById(Integer id) {
        Movies movie = movieDao.findOne(id);
        MovieDTO movieDTO = beanMapper.singleMapper(movie, MovieDTO.class);
        return movieDTO;
    }


    @Override
    public void delete(Integer key) {
        movieDao.deleteOne(key);
    }

    @Override
    public void update(MovieDTO entity) {
        Movies movies = beanMapper.singleMapper(entity, Movies.class);
        movieDao.update(movies);
    }

}
