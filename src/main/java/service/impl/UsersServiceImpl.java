package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.UsersDTO;
import mapper.BeanMapper;
import model.Users;
import service.api.Service;

import java.util.List;

/**
 * Created by den4ik on 16.06.2016.
 */
public class UsersServiceImpl implements Service<Integer, UsersDTO> {
    private static UsersServiceImpl service;
    private Dao<Integer, Users> usersDao;
    private BeanMapper beanMapper;

    private UsersServiceImpl() {
        usersDao = DaoFactory.getInstance().getUsersDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized UsersServiceImpl getInstance() {
        if (service == null) {
            service = new UsersServiceImpl();
        }
        return service;
    }

    @Override
    public List<UsersDTO> getAll() {
        List<Users> usersList = usersDao.findAll();
        List<UsersDTO> usersDTOs = beanMapper.listMapToList(usersList, UsersDTO.class);
        return usersDTOs;
    }

    @Override
    public UsersDTO getById(Integer id) {
        Users users = usersDao.findOne(id);
        UsersDTO usersDTO = beanMapper.singleMapper(users, UsersDTO.class);
        return usersDTO;
    }

    @Override
    public void save(UsersDTO entity) {
        Users users = beanMapper.singleMapper(entity, Users.class);
        usersDao.save(users);
    }

    public UsersDTO getByLogin(String value){
        Users users = usersDao.getBy("login", value);
        UsersDTO usersDTO = beanMapper.singleMapper(users, UsersDTO.class);
        return usersDTO;
    }

    @Override
    public void delete(Integer key) {
        usersDao.deleteOne(key);
    }

    @Override
    public void update(UsersDTO entity) {
        Users users = beanMapper.singleMapper(entity, Users.class);
        usersDao.update(users);
    }
}
