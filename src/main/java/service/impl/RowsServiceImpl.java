package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.RowsDTO;
import mapper.BeanMapper;
import model.Rows;
import service.api.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by den4ik on 16.06.2016.
 */
public class RowsServiceImpl implements Service<Integer, RowsDTO> {
    private static RowsServiceImpl service;
    private Dao<Integer, Rows> rowsDao;
    private BeanMapper beanMapper;

    private RowsServiceImpl() {
        rowsDao = DaoFactory.getInstance().getRowsDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized RowsServiceImpl getInstance() {
        if (service == null) {
            service = new RowsServiceImpl();
        }
        return service;
    }

    @Override
    public List<RowsDTO> getAll() {
        List<Rows> rowsList = rowsDao.findAll();
        List<RowsDTO> rowsDTOs = beanMapper.listMapToList(rowsList, RowsDTO.class);
        return rowsDTOs;
    }

    @Override
    public RowsDTO getById(Integer id) {
        Rows rows = rowsDao.findOne(id);
        RowsDTO rowsDTO = beanMapper.singleMapper(rows, RowsDTO.class);
        return rowsDTO;
    }

    public List<RowsDTO> getByHallID (String value){
        List<RowsDTO> rowsList = new ArrayList<>();
        Integer intValue = Integer.parseInt(value);
        for (RowsDTO rowsDTO : getAll()) {
            if(rowsDTO.getHallID() == intValue){
                rowsList.add(rowsDTO);
            }
        }
        return rowsList;
    }

    @Override
    public void save(RowsDTO entity) {
        Rows rows = beanMapper.singleMapper(entity, Rows.class);
        rowsDao.save(rows);
    }

    @Override
    public void delete(Integer key) {
        rowsDao.deleteOne(key);
    }

    @Override
    public void update(RowsDTO entity) {
        Rows rows = beanMapper.singleMapper(entity, Rows.class);
        rowsDao.update(rows);
    }
}
