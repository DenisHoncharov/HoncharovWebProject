package controllers;

import dto.UsersDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static dto.UsersDTO.Role.ADMIN;
import static dto.UsersDTO.Role.USER;

/**
 * Created by den4ik on 19.06.2016.
 */
@WebServlet(name = "PersonalAreaServlet", urlPatterns = "/pages/users/personalAreaPage")
public class PersonalAreaServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsersDTO usersDTO = (UsersDTO) request.getSession().getAttribute("user");
        request.setAttribute("usersDTO", usersDTO);
        if(usersDTO.getRole() == USER) {
            request.getRequestDispatcher("personalAreaPage.jsp").forward(request, response);
        }else if(usersDTO.getRole() == ADMIN){
            response.sendRedirect("/app/pages/admin/admin.jsp");
        }
    }
}
