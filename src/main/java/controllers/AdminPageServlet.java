package controllers;

import dto.UsersDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by den4ik on 27.06.2016.
 */
@WebServlet(name = "AdminPageServlet", urlPatterns = "/pages/admin/admin")
public class AdminPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsersDTO usersDTO = (UsersDTO) request.getSession().getAttribute("user");
        request.setAttribute("usersDTO", usersDTO);
        request.getRequestDispatcher("admin.jsp").forward(request, response);
    }
}
