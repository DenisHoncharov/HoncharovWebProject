package controllers;

import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by den4ik on 28.06.2016.
 */
@WebServlet(name = "DeleteSessionsServlet", urlPatterns = "/pages/admin/deleteSessions")
public class DeleteSessionsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SessionServiceImpl.getInstance().delete(Integer.parseInt(request.getParameter("id")));
        request.getRequestDispatcher("changeSessions.jsp").forward(request, response);
    }
}
