package controllers;

import dto.RowsDTO;
import dto.SessionDTO;
import service.impl.RowsServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by den4ik on 20.06.2016.
 */
@WebServlet(name = "SessionServlet", urlPatterns = "/pages/users/session")
public class SessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("session_id")));
        List<RowsDTO> rowsDTOs = RowsServiceImpl.getInstance().getByHallID(request.getParameter("hall_id"));
        request.setAttribute("rowsDTOs", rowsDTOs);
        request.setAttribute("sessionDTO", sessionDTO);
        request.getRequestDispatcher("session.jsp").forward(request, response);
    }
}
