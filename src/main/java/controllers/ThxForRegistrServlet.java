package controllers;

import MyExceptions.LoginLengthExceptions;
import dto.UsersDTO;
import service.impl.UsersServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

import static dto.UsersDTO.Role.USER;

/**
 * Created by den4ik on 20.06.2016.
 */
@WebServlet(name = "ThxForRegistrServlet", urlPatterns = "/thxForRegistr")
public class ThxForRegistrServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = null;
        try {
            String login1 = request.getParameter("login");
            if (login1.length() < 5) {
                throw new LoginLengthExceptions();
            }
            login = login1;
            String password = request.getParameter("password");
            String firstname = request.getParameter("firstname");
            String lastname = request.getParameter("lastname");
            String email = request.getParameter("email");
            UsersDTO.Sex sex = UsersDTO.Sex.valueOf(request.getParameter("gender"));
            LocalDate birthday = LocalDate.of(Integer.parseInt(request.getParameter("year")), Integer.parseInt(request.getParameter("month")), Integer.parseInt(request.getParameter("day")));

            UsersServiceImpl.getInstance().save(new UsersDTO(login, password, firstname, lastname, email, sex, birthday, USER));

            request.getRequestDispatcher("pages/common/homepage.jsp").forward(request, response);
        } catch (LoginLengthExceptions e){
            request.getRequestDispatcher("/pages/common/registrationError.jsp").forward(request, response);
        }
    }
}
