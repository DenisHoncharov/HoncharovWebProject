package dto;

import model.Entity;

/**
 * Created by den4ik on 04.06.2016.
 */
public class TicketsDTO extends Entity<Integer> {
    private int userID;
    private int sessionID;
    private int rowNumber;
    private int seatNumber;
    private boolean isSold;

    public TicketsDTO() {
    }

    public TicketsDTO(int userID, int sessionID, int rowNumber, int seatNumber, boolean isSold) {
        setUserID(userID);
        setSessionID(sessionID);
        setRowNumber(rowNumber);
        setSeatNumber(seatNumber);
        setSold(isSold);
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getSessionID() {
        return sessionID;
    }

    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return isSold;
    }

    public void setSold(boolean sold) {
        isSold = sold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TicketsDTO)) return false;
        if (!super.equals(o)) return false;

        TicketsDTO ticketsDTO = (TicketsDTO) o;

        if (getUserID() != ticketsDTO.getUserID()) return false;
        if (getSessionID() != ticketsDTO.getSessionID()) return false;
        if (getRowNumber() != ticketsDTO.getRowNumber()) return false;
        if (getSeatNumber() != ticketsDTO.getSeatNumber()) return false;
        return isSold() == ticketsDTO.isSold();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getUserID();
        result = 31 * result + getSessionID();
        result = 31 * result + getRowNumber();
        result = 31 * result + getSeatNumber();
        result = 31 * result + (isSold() ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "userID=" + getUserID() +
                ", sessionID=" + getSessionID() +
                ", rowNumber=" + getRowNumber() +
                ", seatNumber=" + getSeatNumber() +
                ", isSold=" + isSold +
                '}' + super.toString();
    }
}
