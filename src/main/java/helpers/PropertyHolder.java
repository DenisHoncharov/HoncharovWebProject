package helpers;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by Kovantonlenko on 4/5/2016.
 */
public final class PropertyHolder { // в этом классе происходит инициализация полей в программе из файла проперти (в конкретном случаи jdbs driver) подключение к бд

    private static PropertyHolder propertyHolder; // static private PH

    private boolean isInMemoryDB;
    private String jdbcUrl;
    private String dbUserLogin;
    private String dbUserPassword;
    private String dbDriver;

    private PropertyHolder() { //при вызове конструктора будет вызываться метод loadProperties()
        loadProperties();
    }

    public static synchronized PropertyHolder getInstance() { //для одноразовой инициализации поля РН что бы мы один раз его вызвали а потом пользовались только одним объектом
        if (propertyHolder == null) {
            propertyHolder = new PropertyHolder();
        }
        return propertyHolder;
    }

    private void loadProperties() {     //в этом методе проходит загрузка файла проперти и инициализация полей
        Properties prop = new Properties();

        try {
            prop.load(PropertyHolder.class.getClassLoader().getResourceAsStream("application.properties"));// мы загружаем в проперти лоад данные из файла "application.properties" через getClassLoader().getResourceAsStream
                                                                                                           // getClassLoader() - знает где лежат файлы, getResourceAsStream - предоставляет данные как поток
            this.isInMemoryDB = Boolean.valueOf(prop.getProperty("isInMemoryDB"));                         // данные лежат в: target\classes\...
            this.dbDriver = prop.getProperty("dbDriver");
            this.jdbcUrl = prop.getProperty("jdbcUrl");
            this.dbUserLogin = prop.getProperty("dbUserLogin");
            this.dbUserPassword = prop.getProperty("dbUserPassword");

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public String getDbUserLogin() {
        return dbUserLogin;
    }

    public String getDbUserPassword() {
        return dbUserPassword;
    }

    public boolean isInMemoryDB() {
        return isInMemoryDB;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }
}